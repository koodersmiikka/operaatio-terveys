-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 27.11.2016 klo 04:01
-- Palvelimen versio: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `terveyssys`
--

-- --------------------------------------------------------

--
-- Rakenne taululle `inputs`
--

CREATE TABLE `inputs` (
  `id` int(11) NOT NULL,
  `module` int(11) NOT NULL,
  `c5user` int(11) NOT NULL,
  `adddate` date NOT NULL,
  `value1` varchar(250) NOT NULL,
  `value2` varchar(250) DEFAULT NULL,
  `value3` varchar(250) DEFAULT NULL,
  `value4` varchar(250) DEFAULT NULL,
  `value5` varchar(250) DEFAULT NULL,
  `value6` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='The actual data submitted to the system';

--
-- Vedos taulusta `inputs`
--

INSERT INTO `inputs` (`id`, `module`, `c5user`, `adddate`, `value1`, `value2`, `value3`, `value4`, `value5`, `value6`) VALUES
(1, 12, 2, '2016-11-24', '57', '12', '12', '80', NULL, NULL),
(2, 12, 2, '2016-11-24', '1138', '0', '0', '1595', NULL, NULL),
(3, 12, 2, '2016-11-25', '210', '97', '101', '295', NULL, NULL),
(4, 12, 2, '2016-11-25', '0', '0', '0', '0', NULL, NULL),
(5, 12, 2, '2016-11-25', '77', '6', '7', '108', NULL, NULL),
(6, 12, 2, '2016-11-25', '9', '0', '0', '14', NULL, NULL),
(7, 12, 2, '2016-11-26', '0', '0', '0', '0', NULL, NULL),
(8, 12, 2, '2016-11-26', '150', '84', '87', '209', NULL, NULL),
(9, 12, 2, '2016-11-26', '209', '26', '27', '293', NULL, NULL),
(10, 12, 2, '2016-11-26', '58', '12', '13', '82', NULL, NULL),
(11, 12, 2, '2016-11-26', '9', '12', '12', '12', NULL, NULL),
(12, 12, 2, '2016-11-26', '167', '27', '29', '234', NULL, NULL),
(13, 12, 2, '2016-11-26', '3', '0', '0', '5', NULL, NULL),
(14, 12, 2, '2016-11-26', '35', '4', '4', '49', NULL, NULL),
(15, 12, 2, '2016-11-26', '2', '0', '0', '3', NULL, NULL),
(16, 12, 2, '2016-11-26', '327', '8', '8', '458', NULL, NULL),
(17, 12, 2, '2016-11-22', '500', '250', '660', '412', NULL, NULL),
(18, 12, 2, '2016-11-21', '550', '550', '1250', '512', NULL, NULL),
(21, 12, 2, '2016-11-17', '580', '2522', '3311', '4100', NULL, NULL),
(22, 12, 2, '2016-11-26', '2', '61', '63', '4', NULL, NULL),
(23, 12, 2, '2016-11-28', '2500', '2600', '1450', '1600', NULL, NULL),
(24, 12, 2, '2016-11-29', '5000', '7000', '8000', '5000', NULL, NULL),
(25, 12, 3, '2016-11-24', '57', '12', '12', '80', NULL, NULL),
(26, 12, 3, '2016-11-24', '1138', '0', '0', '1595', NULL, NULL),
(27, 12, 3, '2016-11-25', '210', '97', '101', '295', NULL, NULL),
(28, 12, 3, '2016-11-25', '0', '0', '0', '0', NULL, NULL),
(29, 12, 3, '2016-11-25', '77', '6', '7', '108', NULL, NULL),
(30, 12, 3, '2016-11-25', '9', '0', '0', '14', NULL, NULL),
(31, 12, 3, '2016-11-26', '0', '0', '0', '0', NULL, NULL),
(32, 12, 3, '2016-11-26', '150', '84', '87', '209', NULL, NULL),
(33, 12, 3, '2016-11-26', '209', '26', '27', '293', NULL, NULL),
(34, 12, 3, '2016-11-26', '58', '12', '13', '82', NULL, NULL),
(35, 12, 3, '2016-11-26', '9', '12', '12', '12', NULL, NULL),
(36, 12, 3, '2016-11-26', '167', '27', '29', '234', NULL, NULL),
(37, 12, 3, '2016-11-26', '3', '0', '0', '5', NULL, NULL),
(38, 12, 3, '2016-11-26', '35', '4', '4', '49', NULL, NULL),
(39, 12, 3, '2016-11-26', '2', '0', '0', '3', NULL, NULL),
(40, 12, 3, '2016-11-26', '327', '8', '8', '458', NULL, NULL),
(41, 12, 3, '2016-11-26', '2', '61', '63', '4', NULL, NULL);

-- --------------------------------------------------------

--
-- Rakenne taululle `modules`
--

CREATE TABLE `modules` (
  `id` int(11) NOT NULL,
  `addtime` datetime NOT NULL,
  `name` varchar(200) NOT NULL,
  `columns` int(11) NOT NULL,
  `data1` varchar(200) NOT NULL,
  `unit1` varchar(50) NOT NULL,
  `data2` varchar(200) DEFAULT NULL,
  `unit2` varchar(50) DEFAULT NULL,
  `data3` varchar(200) DEFAULT NULL,
  `unit3` varchar(50) DEFAULT NULL,
  `data4` varchar(200) DEFAULT NULL,
  `unit4` varchar(50) DEFAULT NULL,
  `data5` varchar(200) DEFAULT NULL,
  `unit5` varchar(50) DEFAULT NULL,
  `data6` varchar(200) DEFAULT NULL,
  `unit6` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vedos taulusta `modules`
--

INSERT INTO `modules` (`id`, `addtime`, `name`, `columns`, `data1`, `unit1`, `data2`, `unit2`, `data3`, `unit3`, `data4`, `unit4`, `data5`, `unit5`, `data6`, `unit6`) VALUES
(12, '2016-11-26 17:25:14', 'Polar Steps', 4, 'Walking', 'meters', 'Running', 'steps', 'Running', 'meters', 'Walking', 'steps', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Rakenne taululle `users`
--

CREATE TABLE `users` (
  `c5user` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `level` int(100) NOT NULL,
  `experience` bigint(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vedos taulusta `users`
--

INSERT INTO `users` (`c5user`, `name`, `level`, `experience`) VALUES
(2, 'Testi K&auml;ytt&auml;j&auml;', 4, 303),
(3, 'Miikka Kostian', 2, 39),
(4, 'Roonas', 0, 0);

-- --------------------------------------------------------

--
-- Rakenne taululle `userStats`
--

CREATE TABLE `userStats` (
  `id` int(11) NOT NULL,
  `c5user` int(11) NOT NULL,
  `experience` int(100) NOT NULL,
  `creationTimeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vedos taulusta `userStats`
--

INSERT INTO `userStats` (`id`, `c5user`, `experience`, `creationTimeStamp`) VALUES
(1, 2, 1, '2016-11-26 19:44:37'),
(2, 2, 16, '2016-11-26 19:44:37'),
(3, 2, 4, '2016-11-26 19:44:37'),
(4, 2, 0, '2016-11-26 19:44:37'),
(5, 2, 1, '2016-11-26 19:44:37'),
(6, 2, 0, '2016-11-26 19:44:37'),
(7, 2, 0, '2016-11-26 19:44:37'),
(8, 2, 3, '2016-11-26 19:44:37'),
(9, 2, 3, '2016-11-26 19:44:37'),
(10, 2, 1, '2016-11-26 19:44:37'),
(11, 2, 0, '2016-11-26 19:44:38'),
(12, 2, 3, '2016-11-26 19:44:38'),
(13, 2, 0, '2016-11-26 19:44:38'),
(14, 2, 1, '2016-11-26 19:44:38'),
(15, 2, 0, '2016-11-26 19:44:38'),
(16, 2, 5, '2016-11-26 19:44:38'),
(17, 2, 0, '2016-11-26 19:44:38'),
(18, 2, 11, '2016-11-26 20:26:15'),
(19, 2, 10, '2016-11-26 21:41:21'),
(20, 2, 15, '2016-11-26 21:50:21'),
(21, 2, 66, '2016-11-27 02:25:59'),
(22, 2, 1, '2016-11-27 02:40:44'),
(23, 2, 0, '2016-11-27 02:40:44'),
(24, 2, 0, '2016-11-27 02:41:49'),
(25, 2, 42, '2016-11-27 02:43:14'),
(26, 2, 120, '2016-11-27 02:44:25'),
(27, 3, 1, '2016-11-27 02:52:57'),
(28, 3, 16, '2016-11-27 02:52:57'),
(29, 3, 4, '2016-11-27 02:52:57'),
(30, 3, 0, '2016-11-27 02:52:58'),
(31, 3, 1, '2016-11-27 02:52:58'),
(32, 3, 0, '2016-11-27 02:52:58'),
(33, 3, 0, '2016-11-27 02:52:58'),
(34, 3, 3, '2016-11-27 02:52:58'),
(35, 3, 3, '2016-11-27 02:52:58'),
(36, 3, 1, '2016-11-27 02:52:58'),
(37, 3, 0, '2016-11-27 02:52:58'),
(38, 3, 3, '2016-11-27 02:52:58'),
(39, 3, 0, '2016-11-27 02:52:58'),
(40, 3, 1, '2016-11-27 02:52:58'),
(41, 3, 0, '2016-11-27 02:52:58'),
(42, 3, 5, '2016-11-27 02:52:58'),
(43, 3, 1, '2016-11-27 02:52:58'),
(44, 3, 0, '2016-11-27 02:52:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `inputs`
--
ALTER TABLE `inputs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module` (`module`,`c5user`),
  ADD KEY `user` (`c5user`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`c5user`);

--
-- Indexes for table `userStats`
--
ALTER TABLE `userStats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `c5user` (`c5user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `inputs`
--
ALTER TABLE `inputs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `userStats`
--
ALTER TABLE `userStats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- Rajoitteet vedostauluille
--

--
-- Rajoitteet taululle `inputs`
--
ALTER TABLE `inputs`
  ADD CONSTRAINT `module` FOREIGN KEY (`module`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user` FOREIGN KEY (`c5user`) REFERENCES `users` (`c5user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Rajoitteet taululle `userStats`
--
ALTER TABLE `userStats`
  ADD CONSTRAINT `c5u` FOREIGN KEY (`c5user`) REFERENCES `users` (`c5user`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
