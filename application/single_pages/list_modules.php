<?php 
include './application/themes/systheme/blocks/config.php';

$getdemmodules = $db->prepare("SELECT * FROM modules");
$getdemmodules->execute();
$modules = $getdemmodules->fetchAll(PDO::FETCH_ASSOC);

// All is echoed, I'm tired don't judge
// atleast it's row by row for readability!
echo '<table class="table table-bordered">';
echo '<thead>';
echo '<tr>';
echo '<td></td>';
echo '<td>Module name</td>';
echo '<td>Columns</td>';
echo '</tr>';
echo '</thead>';
echo '<tbody>';
for($i=0;$i<count($modules);$i++){
    echo '<tr>';
    echo '<td><a href="'.View::url('/edit_module').'?id='.$modules[$i]["id"].'" class="btn btn-success">Edit</a>';
    echo '<td>'.$modules[$i]["name"].'</td>';
    echo '<td>'.$modules[$i]["columns"].'</td>';
    echo '</tr>';
}
echo '</tbody>';
echo '</table>';
?>