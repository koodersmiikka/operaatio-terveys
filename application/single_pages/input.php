<?php 
include './application/themes/systheme/blocks/config.php';

if($_POST["module"]){
    $getmoduleinfo = $db->prepare("SELECT * FROM modules WHERE id = :id");
    $getmoduleinfo->bindParam(':id', $_POST["module"], PDO::PARAM_INT);
    $getmoduleinfo->execute();
    $module = $getmoduleinfo->fetch(PDO::FETCH_ASSOC);
    echo "<h1>".$module['name'].'</h1>
    <h3>Add manually</h3>
    <form action="'.View::url('/tools/addInput').'" method="POST">';
    echo 'Date<input type="date" name="adddate" class="form-control"><br>';
    for($i=1;$i<$module["columns"]+1;$i++){
        echo $module["data".$i];
        if($module["unit".$i]){
        echo " (".$module['unit'.$i].")";
        }
        echo '<input name="value'.$i.'" class="form-control"><br>';
    }
    ?>
    <input type="hidden" name="module" value="<?php echo $_POST["module"]; ?>">
    <input type="submit" value="Submit" class="btn btn-primary">
</form>
    <div class="text-center">
        <hr>
        <h1>OR</h1>
        <hr>
    </div>
<form action="<?php echo View::url('/tools/addInputCSV'); ?>" method="POST" enctype="multipart/form-data">
    <h3>Insert as CSV</h3>
    <input id="fileSelect" name="fileToUpload" type="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" /> <br>
    <input type="hidden" name="module" value="<?php echo $_POST["module"]; ?>">
    <input type="submit" class="btn btn-primary" value="Submit">
</form>
<?php }else{

$getdemmodules = $db->prepare("SELECT * FROM modules");
$getdemmodules->execute();
$modules = $getdemmodules->fetchAll(PDO::FETCH_ASSOC);
?>
<div class="col-md-5">
    <h1>Add input</h1>
    Add data to the system
    <hr>
    <h3>How to get data directly from Wellmo</h3>
    If you have your Wellmo account login at <br><br><a href="http://my.wellmo.com" target="_blank">http://my.wellmo.com</a><br><br>
    After logging in get your latest activity from:
    <a href="https://my.wellmo.com/rest/user/wellnessdata/all/csv" target="_blank">https://my.wellmo.com/rest/user/wellnessdata/all/csv</a><br>
    You can upload your pedometer data directly from the next page at the bottom of the page! ('Insert as CSV')
</div>

<div class="col-md-7">
    <form action="<?php echo View::url('/input'); ?>" method="POST">
        What data do you want to save?
        <select name="module" class="form-control">
            <?php 
            for($i=0;$i<count($modules);$i++){
                echo '<option value="'.$modules[$i]["id"].'">'.$modules[$i]["name"].' ('.$modules[$i]["columns"].' values)</option>';
            }
            ?>
        </select><br>
        <input type="submit" class="btn btn-primary" value="Select">
    </form>
</div>
<?php 
} // If no module is selected
 ?>