<?php if($_POST["columns"]){ 
echo '<form action="'.View::url('/tools/createModule').'" method="POST">';
for($i=0;$i<$_POST["columns"];$i++){
    $round = $i+1;
    echo 'Data '.$round.' name<input type="text" name="data'.$round.'" class="form-control" placeholder="i.e. Weigth, excercise type, steps"><br>';
    echo 'Data '.$round.' measurement<input type="text" name="measurement'.$round.'" class="form-control" placeholder="i.e. kg, biking, steps"><hr>';
}
echo '<input type="hidden" name="columnamount" value="'.$_POST["columns"].'">';
echo '<input type="hidden" name="modulename" value="'.$_POST["name"].'">';
echo '<input type="submit" class="btn btn-primary" value="Submit"></form>';
}else{ ?>
<div class="col-md-5">
    <h1>Create Module</h1>
    This page helps you to create a new module to insert in to the system
</div>

<div class="col-md-7">
    <form action="<?php echo View::url('/create_module'); ?>" method="POST">
        Name of the module
        <input type="text" class="form-control" name="name" placeholder="i.e. Steps, Waist, Weigth" required><br>
        How many columns of data
        <input type="number" class="form-control" name="columns" min="1" max="6" placeholder="Number of things to record" required><br>
        <input type="submit" class="btn btn-primary" value="Submit">
    </form>
        <div class="text-center">
            <hr>
            <h1>OR</h1>
            <hr>
        </div>
    <form action="<?php echo View::url('/tools/createModuleCSV'); ?>" method="POST" enctype="multipart/form-data">
        Insert example CSV
        <input id="fileSelect" type="file" name="fileToUpload" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" /> <br>
        <input type="submit" class="btn btn-primary" value="Submit">
    </form>
</div>
<?php } // IF column amount form not sent ?>