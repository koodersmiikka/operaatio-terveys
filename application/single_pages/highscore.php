<?php include './application/themes/systheme/blocks/config.php';
include_once './application/tools/userStats.php';
$getuserinfo = $db->prepare("SELECT * FROM users ORDER BY experience DESC");
$getuserinfo->execute();
$userinfo = $getuserinfo->fetchAll(PDO::FETCH_ASSOC);
?>

<div class="col-md-8 col-md-offset-2 text-center">
    <table class="table">
        <thead>
            <tr>
                <td>Player</td><td>Current Level</td><td>XP to next level</td>
            </tr>
        </thead>
        <tbody>
            <?php 
            for($i=0;$i<count($userinfo);$i++){
                echo '<tr>';
                echo '<td>'.$userinfo[$i]["name"].'</td><td>'.calculateLevels($userinfo[$i]["experience"]).'</td><td>'.round(calculateExpToNextLevel($userinfo[$i]["experience"]),0).'</td>';
                echo '</tr>';
            }
             ?>
        </tbody>
    </table>
</div>