<?php

/**
 * -----------------------------------------------------------------------------
 * Generated 2016-11-27T05:24:44+01:00
 *
 * DO NOT EDIT THIS FILE DIRECTLY
 *
 * @item      misc.modern_tile_thumbnail_bgcolor
 * @group     concrete
 * @namespace null
 * -----------------------------------------------------------------------------
 */
return array(
    'site' => 'Enter Prener',
    'version_installed' => '5.7.5.9',
    'misc' => array(
        'access_entity_updated' => 1479986949,
        'login_redirect' => 'HOMEPAGE',
        'login_redirect_cid' => 0,
        'login_admin_to_dashboard' => 1,
        'do_page_reindex_check' => false,
        'favicon_fid' => '1',
        'iphone_home_screen_thumbnail_fid' => '1',
        'modern_tile_thumbnail_fid' => '1',
        'modern_tile_thumbnail_bgcolor' => '',
    ),
    'cache' => array(
        'blocks' => false,
        'assets' => false,
        'theme_css' => false,
        'overrides' => false,
        'pages' => '0',
        'full_page_lifetime' => 'default',
        'full_page_lifetime_value' => null,
    ),
    'theme' => array(
        'compress_preprocessor_output' => false,
        'generate_less_sourcemap' => false,
    ),
    'external' => array(
        'news_overlay' => false,
        'news' => false,
    ),
    'seo' => array(
        'canonical_url' => '',
        'canonical_ssl_url' => '',
        'redirect_to_canonical_url' => 0,
        'url_rewriting' => 1,
    ),
    'debug' => array(
        'detail' => 'debug',
        'display_errors' => true,
    ),
    'user' => array(
        'registration' => array(
            'email_registration' => true,
            'type' => 'enabled',
            'captcha' => false,
            'enabled' => true,
            'validate_email' => false,
            'approval' => false,
            'notification' => null,
            'notification_email' => false,
        ),
    ),
);
