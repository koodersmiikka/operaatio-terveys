<?php

return array(
    'default-connection' => 'concrete',
    'connections' => array(
        'concrete' => array(
            'driver' => 'c5_pdo_mysql',
            'server' => 'localhost',
            'database' => 'terveysc5',
            'username' => 'terveysc5',
            'password' => 'terveys',
            'charset' => 'utf8',
        ),
    ),
);
