<?php include_once 'blocks/header.php';
$u = new User();
if($currentuser || !$u->isLoggedIn()){




$getlabels = $db->prepare("SELECT * FROM modules WHERE id = 12");
$getlabels->execute();
$labels = $getlabels->fetch(PDO::FETCH_ASSOC);

// get polar steps stuff
$getinput = $db->prepare("SELECT id, adddate, SUM(value1) AS val1, SUM(value2) AS val2, SUM(value3) AS val3, SUM(value4) AS val4 FROM inputs WHERE module = 12 AND c5user = :c5u GROUP BY adddate");
$getinput->bindParam(':c5u', $u->getUserID(), PDO::PARAM_INT);
$getinput->execute();
$steps = $getinput->fetchAll(PDO::FETCH_ASSOC);

echo '<div id="weeklyExp" style="min-width: 310px; height: 600px; margin: 0 auto"></div>';

	
	 ?>
    
	
<script type="text/javascript">
    $(function () {
      Highcharts.chart('weeklyExp', {
          chart: {
              type: 'line'
          },
          title: {
              text: 'Pedometer activity'
          },
          subtitle: {
              text: 'From your 14 last active days'
          },
          xAxis: {
            categories: [<?php 
                for($i=0;$i<count($steps);$i++){
                    echo "'".date('d.m', strtotime($steps[$i]["adddate"]))."',";
                }
                ?>]
        },
          yAxis: {
              title: {
                  text: 'Steps'
              },
              labels: {
                  formatter: function () {
                      return this.value;
                  }
              }
          },
          tooltip: {
              crosshairs: true,
              shared: true
          },
          plotOptions: {
              spline: {
                  marker: {
                      radius: 4,
                      lineColor: '#666666',
                      lineWidth: 1
                  }
              }
          },
		  series: [
			<?php 
			for($i=1;$i<$labels["columns"]+1;$i++){
				echo '{';
				echo 'name: "'.$labels["data".$i].' '.$labels["unit".$i].'",';
				echo 'data: [';
				for($k=0;$k<count($steps);$k++)	{
					echo $steps[$k]["val".$i].",";
				}
				echo ']';
				echo '},';
			}
			?>
			]
      });
  });
</script>
        
<?php 
}else{ ?>
	<form action="<?php echo View::url('/tools/userInitial'); ?>" method="POST">
		What's your name?
		<input type="text" class="form-control" name="name"><br>
		<input type="submit" class="btn btn-primary" value="Submit">
	</form>
<?php }
include_once 'blocks/footer.php'; ?>