<?php
// Yleistä
$hc_path				= View::url('');
//$hidden_fileupload_path	= "/home/isannointipilvi/huoneistokuvat";
//$fileupload_path		= "/home/isannointipilvi/public_html/application/huoneistokuvat";

include_once 'globals.php';

// Tietokanta
$dbname = "terveyssys";
$dbuser = "terveyssys";
$dbpw	= "terveys";

$c5dbname	= 'terveysc5';
$c5user		= 'terveysc5';
$c5pw		= 'terveys';

//Sivujen tietokanta yhdistys
try
  {
        $dsn = 'mysql:host=localhost;dbname='.$dbname;
        $username = $dbuser;
        $password = $dbpw;
        $options = array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
        ); 

        $db = new PDO($dsn, $username, $password, $options);
  }

catch(Exception $e)
  {
  echo 'Message: ' .$e->getMessage();
  }

  // Haetaan kirjautuneen käyttäjän tiedot
  $u = new User();
  $getuserinfo = $db->prepare("SELECT * FROM users WHERE c5user = :id");
  $getuserinfo->bindParam(':id', $u->getUserID(), PDO::PARAM_INT);
  $getuserinfo->execute();
  $currentuser = $getuserinfo->fetch(PDO::FETCH_ASSOC);

?>