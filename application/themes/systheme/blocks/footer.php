</div><!-- turbo wrapper -->
</div><!-- concrete5 wrapper -->

</body>

<script>


        function calculateLevels(currentExp) {
         nextLevel = -1;
         level = 0;

         while(nextLevel < currentExp){
             nextLevel += Math.pow((level+1),1.1)+30*Math.pow((level+1),1.1)+30*(level+1)-50;
             level++;
         }
         return level;
        }


        var ctx = document.getElementById('canvas').getContext('2d');
        var lvlup = new Image();
        var gradient = ctx.createLinearGradient(0, 0, 0, canvas.height);
<?php if(isset($_GET["oldxp"]) && isset($_GET["newxp"])){ ?>
        var e0 = <?php echo $_GET["oldxp"]; ?>;
        var e1 = <?php echo $_GET["newxp"]; ?>;
<?php } ?>
        var curlevel = 0;
        var levelup = 0;

        function init(){
          lvlup.src = '<?php echo $this->getThemePath()?>/img/lvl_up.png';

          gradient.addColorStop("0","#FF0");
          gradient.addColorStop("0.8","#0FF");
          gradient.addColorStop("1.0","#FFF");
          curlevel = calculateLevels(e0);
          draw(0);
        }

        function draw(f) {

          ctx.clearRect(0, 0, canvas.width, canvas.height);

          ctx.fillStyle = 'rgba(80,100,120,1)';
          ctx.fillRect(0,0, canvas.width*f,canvas.height); // Shadow

          var newLevel = calculateLevels(e0 + e1*f);
          if(newLevel > curlevel){
              curlevel = newLevel;
              levelup = 1;
          }

          ctx.fillStyle = gradient;
          ctx.font="22px Verdana";
          ctx.textAlign="end";

          if(levelup > 0){
              levelup -= 0.1;
              ctx.save();
              ctx.translate(Math.sin(f*1000)*canvas.width*0.01,0);
              ctx.fillText("lvl " + curlevel, canvas.width*0.95, canvas.height*0.4);
              ctx.restore();
          } else {
              ctx.fillText("lvl " + curlevel, canvas.width*0.95, canvas.height*0.4);
          }

          ctx.save();
          ctx.translate(f*canvas.width - canvas.width*0.1,0);
          // Fill with gradient
          ctx.font="30px Verdana";
          ctx.textAlign="center";
          ctx.fillText("" + Math.round(e0 + e1*f) ,0, canvas.height*0.8);

          if(levelup > 0 && f < 1){
              ctx.drawImage(lvlup, 0, canvas.height*levelup, 32, 32);
          }

          ctx.restore();

          if(f < 1){
            setTimeout(function(){draw(f+0.005);}, 20);  
        } else{
            setTimeout(function(){$('#canvas').fadeOut();},3000)
        }

        }
        <?php if(isset($_GET["oldxp"])){ ?>
        $('#canvas').show();
        init();
        <?php } ?>
    </script>

<script src="<?php echo $this->getThemePath()?>/js/bootstrap.min.js"></script>

<?php 
if(!$u->isLoggedIn() || $u->isSuperUser()){
Loader::element('footer_required'); 
}
?>
</html>