<!DOCTYPE html>
<html lang="en">

<head>
<?php
Loader::element('header_required');
use Concrete\Core\Validation\CSRF\Token;
include './application/themes/systheme/blocks/config.php';
$page = Page::getCurrentPage();
if($page->getCollectionName() != "Highscore"){
include './application/tools/userStats.php'; } ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <!-- Bootstrap Core CSS -->
    <link href="<?php echo $this->getThemePath()?>/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <!-- Custom CSS -->
    <link href="<?php echo $this->getThemePath()?>/css/kooders.css" rel="stylesheet" type="text/css">
    <?php if(!$u->isSuperUser()){ ?>
    <!-- jQuery -->
    <script src="<?php echo $this->getThemePath()?>/js/jQuery-2.2.0.min.js"></script>
    <!-- Highcharts -->
    <script src="<?php echo $this->getThemePath()?>/js/highcharts/highcharts.js"></script>
    <script src="<?php echo $this->getThemePath()?>/js/highcharts/modules/data.js"></script>
    <script src="<?php echo $this->getThemePath()?>/js/highcharts/modules/drilldown.js"></script>
    <?php } ?>

</head>

<?php

$currentExp = $currentuser['experience'];

$nextLevel = -1;
$level = 0;

while($nextLevel < $currentExp){
    $nextLevel += pow(($level+1),0.5)+30*pow(($level+1),0.5)+30*($level+1)-50;
    $level++;
}

$experienceToNextLevel = $nextLevel - $currentExp;
$percentToLevel = ($experienceToNextLevel / $nextLevel)*100;

// echo("Current Level: ".calculateLevels($currentuser["experience"]). "<br>");
// echo("Current XP: " .$currentExp. "<br>");
// echo("Next Level: " .$nextLevel. "<br>");
// echo("Experience to next level: " .$experienceToNextLevel. "<br>");
// echo("Percent to next Level: " .$percentToLevel. "<br>");

 ?>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">
	<div class="<?php echo $c->getPageWrapperClass()?>">
        <?php if($u->isLoggedIn()){ ?>
        <div class="progress">
          <div class="progress-bar" role="progressbar" aria-valuenow="70"
          aria-valuemin="0" aria-valuemax="100" style="width:<?php echo 100-$percentToLevel; ?>%">
            <?php echo "(".round($currentExp,0)." / ".round($nextLevel,0)." XP) ".round(100-$percentToLevel,0); ?>%
          </div>
        </div>
        <?php } ?>
        <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a style="padding: 10px;" class="navbar-brand" href="<?php echo $hc_path; ?>"><img style="height:30px;display:inline-block;" src="<?php echo $this->getThemePath()?>/img/logo.png"> Prener</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
        <li><a href="<?php echo View::url('/highscore') ?>">High Scores!</a></li>
    </ul>
      <ul class="nav navbar-nav navbar-right">
         <?php if($u->isLoggedIn()){ ?>
        <li class="navbar-text"><?php echo "Welcome ".$currentuser['name']." [Level: ".calculateLevels($currentuser["experience"])."]" ?></li>
        <?php } ?>
        <li><a href="<?php echo View::url('/') ?>">Dashboard</a></li>
        <li><a href="<?php echo View::url('/input') ?>">Add input</a></li>
        <li><?php
          if (!id(new User)->isLoggedIn()) {
              ?>
              <a href="<?php echo URL::to('/login')?>">
                  <?php echo t('Log in') ?>
              </a>
              <?php
          } else {
              $token = new Token();
              ?>
              <form style="margin-top: 15px;" action="<?php echo URL::to('/login', 'logout') ?>">
                  <?php id(new Token())->output('logout'); ?>
                  <a href="#" onclick="$(this).closest('form').submit();return false"></i> 
                      <?php echo t('Log out') ?>
                  </a>
              </form>
              <?php
          }
          ?></li>
        <?php /*<li><a href="<?php echo View::url('/list_modules') ?>">List Modules</a></li>
        <li><a href="<?php echo View::url('/create_module') ?>">Create Module</a></li> */ ?>
        <!--li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
      </li-->
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<div class="container">
<canvas id="canvas" width="600" height="90"></canvas>