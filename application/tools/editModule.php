<?php
include './application/themes/systheme/blocks/config.php';

$sql = "UPDATE modules SET name = :name, data1 = :data1, data2 = :data2, data3 = :data3, data4 = :data4, data5 = :data5, data6 = :data6, 
unit1 = :unit1, unit2 = :unit2, unit3 = :unit3, unit4 = :unit4, unit5 = :unit5, unit6 = :unit6 WHERE id = :id";
$q = $db->prepare($sql);
$q->execute(array(
    ':name'=>$_POST["modulename"],
    ':data1'=>$_POST["data1"],
    ':unit1'=>$_POST["measurement1"],
    ':data2'=>$_POST["data2"],
    ':unit2'=>$_POST["measurement2"],
    ':data3'=>$_POST["data3"],
    ':unit3'=>$_POST["measurement3"],
    ':data4'=>$_POST["data4"],
    ':unit4'=>$_POST["measurement4"],
    ':data5'=>$_POST["data5"],
    ':unit5'=>$_POST["measurement5"],
    ':data6'=>$_POST["data6"],
    ':unit6'=>$_POST["measurement6"],
    ':id'=>$_POST["id"]
));

header('Location:'.View::url('/edit_module')."?id=".$_POST["id"]."&success=true");
die();

?>