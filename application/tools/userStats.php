<?php
include './application/themes/systheme/blocks/config.php';

function transformSteps($steps) {
  $stepExp = $steps / 100;
  return $stepExp;
}

function calculateLevels($currentExp) {
  $nextLevel = -1;
  $level = 0;
  
  while($nextLevel < $currentExp){
      $nextLevel += pow(($level+1),1.1)+30*pow(($level+1),1.1)+30*($level+1)-50;
      $level++;
  }
  
  return $level;
}

function calculateExpToNextLevel($currentExp){
    $nextLevel = -1;
    $level = 0;
    
    while($nextLevel < $currentExp){
        $nextLevel += pow(($level+1),1.1)+30*pow(($level+1),1.1)+30*($level+1)-50;
        $level++;
    }
    
    return $nextLevel;
}

 ?>