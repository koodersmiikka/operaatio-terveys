<?php
include './application/themes/systheme/blocks/config.php';

$u = new User();
$sql = "INSERT INTO users (c5user, name) VALUES (:c5u, :name)";
$q = $db->prepare($sql);
$q->execute(array(
    ':c5u'=>$u->getUserID(),
    ':name'=>$_POST["name"]
));

header('Location:'.View::url('/').'?register=true');
die();

?>