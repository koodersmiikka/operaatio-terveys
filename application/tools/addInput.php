<?php
include './application/themes/systheme/blocks/config.php';
include './application/tools/userStats.php';
$u = new User();

$oldxp = $currentuser["experience"];

//Add the entered steps and meters to inputs
$sql = "INSERT INTO inputs (module, c5user, adddate, value1, value2, value3, value4, value5, value6) VALUES (:module, :c5user, :adddate, :value1, :value2, :value3, :value4, :value5, :value6)";
$q = $db->prepare($sql);
$q->execute(array(
    ':adddate'=>$_POST["adddate"],
    ':module'=>$_POST["module"],
    ':c5user'=>$u->getUserID(),
    ':value1'=>$_POST["value1"],
    ':value2'=>$_POST["value2"],
    ':value3'=>$_POST["value3"],
    ':value4'=>$_POST["value4"],
    ':value5'=>$_POST["value5"],
    ':value6'=>$_POST["value6"]
));

$overallsteps = $_POST["value2"]+$_POST["value4"];
$expfromthis = transformSteps($overallsteps);

// Add the row to userStats
$sql = "INSERT INTO userStats (c5user, experience) VALUES (:c5u, :expi)";
$q = $db->prepare($sql);
$q->execute(array(
    ':c5u'=>$u->getUserID(),
    ':expi'=>$expfromthis
));

// Calculate overall XP and level for the userStats
$getcurrentxp = $db->prepare("SELECT SUM(experience) AS overallxp FROM userStats WHERE c5user = :c5u");
$getcurrentxp->bindParam(':c5u', $u->getUserID(), PDO::PARAM_INT);
$getcurrentxp->execute();
$currentexpajuttutiedatkos = $getcurrentxp->fetch(PDO::FETCH_ASSOC);

// Add the latest xp and level info to user table
$sql = "UPDATE users SET experience = :expa, level = :level WHERE c5user = :c5u";
$q = $db->prepare($sql);
$q->execute(array(
    ':c5u'=>$u->getUserID(),
    ':expa'=>$currentexpajuttutiedatkos["overallxp"],
    ':level'=>calculateLevels($currentexpajuttutiedatkos["overallxp"])
));

$gainedxp = $currentexpajuttutiedatkos["overallxp"]-$oldxp;
header('Location:'.View::url('/').'?oldxp='.$oldxp."&newxp=".$gainedxp);
die();

?>