<?php
include './application/themes/systheme/blocks/config.php';
include './application/tools/userStats.php';

$oldxp = $currentuser["experience"];

$target_dir = "D:/xampp/htdocs/terveys/application/files/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);

if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
    $csv = array_map('str_getcsv', file($target_file));

for($k=1;$k<count($csv);$k++){
    $array = array();
    $tadaa = explode(';',$csv[$k][0]);

    if($tadaa[0] == 'steps'){
    for($i=0;$i<count($csv[$k]);$i++){
        if($i==0){
            // Time
            $marktime = explode(';',$csv[$k][$i]);
            $piupau = explode('=',$marktime[3]);
            $array[] = date('Y-m-d H:i:s',strtotime($marktime[1]));
            $array[] = $piupau[1];
        }
        elseif($i==3){
            $temppu = explode('}',$csv[$k][$i]);
            $salatemppu = explode('=',$temppu[0]);
            $array[] = $salatemppu[1];
        }else{
            $temppu = explode('=',$csv[$k][$i]);
            $array[] = $temppu[1];
        }
    }
    }
    
    $u = new User();

    $getinput = $db->prepare("SELECT * FROM inputs WHERE adddate = :adddate AND c5user = :c5user AND module = :module AND value1 = :value1 AND value2 = :value2 AND value3 = :value3 AND value4 = :value4");
    $getinput->bindParam(':adddate', date('Y-m-d',strtotime($array[0])), PDO::PARAM_INT);
    $getinput->bindParam(':c5user', $u->getUserID(), PDO::PARAM_INT);
    $getinput->bindParam(':module', $_POST["module"], PDO::PARAM_INT);
    $getinput->bindParam(':value1', $array[1], PDO::PARAM_INT);
    $getinput->bindParam(':value2', $array[2], PDO::PARAM_INT);
    $getinput->bindParam(':value3', $array[3], PDO::PARAM_INT);
    $getinput->bindParam(':value4', $array[4], PDO::PARAM_INT);
    $getinput->execute();
    $steps = $getinput->fetch(PDO::FETCH_ASSOC);
    
    // Only add the rows taht don't exist yet
    if(!$steps){
        $sql = "INSERT INTO inputs (adddate, c5user, module, value1, value2, value3, value4) VALUES (:adddate, :c5user, :module, :value1, :value2, :value3, :value4)";
        $q = $db->prepare($sql);
        $q->execute(array(
            ':adddate'=>$array[0],
            ':c5user'=>$u->getUserID() ,
            ':module'=>$_POST["module"],
            ':value1'=>$array[1],
            ':value2'=>$array[2],
            ':value3'=>$array[3],
            ':value4'=>$array[4],
        ));
        
        $overallsteps = $array[2]+$array[4];
        echo $overallsteps."<br>";
        $expfromthis = transformSteps($overallsteps);
        
        //Add the row to userStats
        $sql = "INSERT INTO userStats (c5user, experience) VALUES (:c5u, :expi)";
        $q = $db->prepare($sql);
        $q->execute(array(
            ':c5u'=>$u->getUserID(),
            ':expi'=>$expfromthis
        ));
    }
    
}

// Calculate overall XP and level for the userStats
$getcurrentxp = $db->prepare("SELECT SUM(experience) AS overallxp FROM userStats WHERE c5user = :c5u");
$getcurrentxp->bindParam(':c5u', $u->getUserID(), PDO::PARAM_INT);
$getcurrentxp->execute();
$currentexpajuttutiedatkos = $getcurrentxp->fetch(PDO::FETCH_ASSOC);

// Add the latest xp and level info to user table
$sql = "UPDATE users SET experience = :expa, level = :level WHERE c5user = :c5u";
$q = $db->prepare($sql);
$q->execute(array(
    ':c5u'=>$u->getUserID(),
    ':expa'=>$currentexpajuttutiedatkos["overallxp"],
    ':level'=>calculateLevels($currentexpajuttutiedatkos["overallxp"])
));

unlink($target_file);
$gainedxp = $currentexpajuttutiedatkos["overallxp"]-$oldxp;
header('Location: '.$hc_path.'?data=added&oldxp='.$oldxp."&newxp=".$gainedxp);
die();
} else {
    echo "Sorry, there was an error uploading your file.";
}

?>