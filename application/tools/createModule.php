<?php
include './application/themes/systheme/blocks/config.php';
//echo "kolumneja ".$_POST["columnamount"]." ja niiden nimi on ".$_POST["modulename"];

$addtime = date('Y-m-d H:i:s');

$sql = "INSERT INTO modules (addtime, name, data1, unit1, columns) VALUES (:addtime, :name, :data1, :unit1, :columns)";
$q = $db->prepare($sql);
$q->execute(array(':addtime'=>$addtime,':name'=>$_POST["modulename"],':data1'=>$_POST["data1"],':unit1'=>$_POST["measurement1"],':columns'=>$_POST["columnamount"]));


$extra_add = $_POST["columnamount"]-1;// How many rounds of data after initial ones

$getnewmoduleinfo = $db->prepare("SELECT * FROM modules WHERE name = :name AND data1 = :data AND unit1 = :unit AND addtime = :addtime");
$getnewmoduleinfo->bindParam(':name', $_POST["modulename"], PDO::PARAM_STR);
$getnewmoduleinfo->bindParam(':data', $_POST["data1"], PDO::PARAM_STR);
$getnewmoduleinfo->bindParam(':unit', $_POST["measurement1"], PDO::PARAM_STR);
$getnewmoduleinfo->bindParam(':addtime', $addtime, PDO::PARAM_STR);
$getnewmoduleinfo->execute();
$newmodule = $getnewmoduleinfo->fetch(PDO::FETCH_ASSOC);

//echo "sitten vielä ".$extra_add;
for($i=0;$i<$extra_add;$i++){
    $inputlabelnumber = $i+2; // Starting from two and stuff
    echo "Lisää kenttä ".$inputlabelnumber;
    $datalabel = "data".$inputlabelnumber;
    $unitlabel = "measurement".$inputlabelnumber;
    $sql = "UPDATE modules SET data".$inputlabelnumber." = :data, unit".$inputlabelnumber." = :unit WHERE id = :id";
    $q = $db->prepare($sql);
    $q->execute(array(':id'=>$newmodule["id"],':data'=>$_POST[$datalabel],':unit'=>$_POST[$unitlabel]));
    echo $sql." ".$datalabel." eli ".$_POST[$datalabel]." ".$unitlabel." eli ".$_POST[$unitlabel]." <br>";
}

header('Location:'.View::url('/'));
die();

?>